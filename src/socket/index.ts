import { Server } from 'socket.io';

const mapRooms = new Map();
const users = new Set();
let users1 = ['q', 'w'];
let users2 = ['a', 's'];

mapRooms.set('Default', users1);
mapRooms.set('Room 2', users2);

// counterMap = new Map( 'numberRoom':  new Map( ['user': 1], [user: 1],.. ) )  // не могу передать 2й new Map / value
// or
// counterMap = new Map( 'numberRoom': [user, user, user] ) // легко передать, сложнее удалить user
// or
// rooms = {}
// users = new Map()
const redirectUser = socket => {
	socket.emit('ERROR_USER', 'User with this name exists');
};

export default (io: Server) => {
	io.on('connection', socket => {
		// todo:проверка на уникальность пользователя | done
		const username = socket.handshake.query.username;
		const isUser = users.has(username);
		if (isUser) {
			redirectUser(socket)
		} else {
			users.add(username);
		}


		socket.on('GET_ROOMS', () => {
			socket.emit("ROOMS", Object.fromEntries(mapRooms));
		})

		socket.on("ADD_ROOM", roomId => {
			// todo: проверка на уникальность комнаты
			//     	- сделано на стороне пользователя
			// todo: добавить комнату - сделано
			// @ts-ignore
			counterMap.set(roomId, []);
			const objRooms = Object.fromEntries(mapRooms);
			const numberOfUsers = 1;

			socket.emit("UPDATE_ROOMS", {roomId, numberOfUsers, objRooms});
		});

		socket.on("JOIN_ROOM", roomId => {
			// todo: войти в комнату
			socket.join(roomId)

			// todo: добавить +1 пользователя в комнату
			// доп. проверка, вдруг не удалился юзер после выхода, то не добавлять
			const usersMap =  mapRooms.get(roomId);
			if (!usersMap.includes(username)) {
				usersMap.push(username);
				mapRooms.set(roomId, usersMap)
			}
			const numberOfUsers = usersMap.length;
			const objRooms = Object.fromEntries(mapRooms);
			console.log(mapRooms);
			socket.emit("UPDATE_ROOMS", {roomId, numberOfUsers, objRooms});
		});

		socket.on("LEAVE_ROOM", roomId => {
			const usersMap = mapRooms.get(roomId)  || [];
			const newUserMap = usersMap.filter( el => el != username)
			mapRooms.set(roomId, newUserMap);

			const numberOfUsers = newUserMap.length;

			if(!numberOfUsers) {
				mapRooms.delete(roomId);
			}

			const objRooms = Object.fromEntries(mapRooms);
			socket.emit("UPDATE_ROOMS", {roomId, numberOfUsers, objRooms});
		})
	});
};
