import {appendRoomElement, removeRoomElement, updateNumberOfUsersInRoom} from "./views/room.mjs";
import {showInputModal, showMessageModal} from "./views/modal.mjs";
import {appendUserElement, changeReadyStatus, removeUserElement} from "./views/user.mjs";

const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('', { query: { username } });
const addRoom = document.getElementById('add-room-btn');
const backRoom = document.getElementById('quit-room-btn');
const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');
const joinRoom = document.getElementById('rooms-wrapper');
const roomName = document.getElementById('room-name');
const ready = document.getElementById('ready-btn');

let activeRoomId = null;
let mapRooms = null;
let readyUser = false;

const setActiveRoomId = roomId => {
	activeRoomId = roomId;
};

socket.on("ERROR_USER", (msg) => {
	alert(`${msg}`);
	sessionStorage.removeItem('username');
	window.location.replace('/login');
})

const start = objRooms => {
	mapRooms = new Map(Object.entries(objRooms));

	mapRooms.forEach((users, name) => {
		appendRoomElement({name, numberOfUsers: users.length})
	})
}

const updateRooms = ({roomId:name, numberOfUsers, objRooms}) => {
	mapRooms = new Map(Object.entries(objRooms));

	if (!numberOfUsers) {
		removeRoomElement(name);
	}

	mapRooms.forEach((users, name) => {
		updateNumberOfUsersInRoom({name, numberOfUsers: users.length})
	})
}

const createRoom = (roomId) => {
	roomId = roomId.trim();
	const isNewRoom = !!mapRooms.get(roomId);

	if (isNewRoom || roomId.length === 0) {
		showMessageModal({message: 'This room already exists or name is empty'})
	} else {
		appendRoomElement({name:roomId, numberOfUsers:0})

		socket.emit('ADD_ROOM', roomId)
		setActiveRoomId(roomId)
		joinRoom.click();
	}
}

const updateUserValueInRoom = () => {

}

const onClickReady = () => {
	readyUser = !readyUser;
	changeReadyStatus({username, ready: readyUser})
	ready.innerHTML = readyUser ? 'NOT READY' : 'READY'
}

const onClickCreateRoom = () => {
	showInputModal({title:'Enter room name', onChange: createRoom})
}

const onClickJoinRoom = () => {
	event.preventDefault();
	const roomId = event.target.getAttribute("data-room-name") || activeRoomId;
	const usersInRoom = mapRooms.get(roomId);
	appendUserElement({username, ready:false, isCurrentUser: true})

	usersInRoom.forEach(username => {
		appendUserElement({username, ready:false, isCurrentUser: false})
	})

	roomsPage.style.display ='none';
	gamePage.style.display ='flex';

	roomName.innerText = `Room name ${roomId}`;
	setActiveRoomId(roomId);
	socket.emit('JOIN_ROOM', roomId)
};

const onClickBackToRooms = () => {
	roomsPage.style.display ='block';
	gamePage.style.display ='none';

	socket.emit("LEAVE_ROOM", activeRoomId);
	removeUserElement(username);
};

addRoom.addEventListener('click', onClickCreateRoom);
backRoom.addEventListener('click', onClickBackToRooms);
joinRoom.addEventListener('click', onClickJoinRoom);
ready.addEventListener('click', onClickReady);

socket.on("ROOMS", start);
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("UPDATE_USER_VALUE", updateUserValueInRoom);
